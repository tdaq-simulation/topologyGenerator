require 'byebug'
require 'spec_helper'
require 'flows_distributions/exponential_distribution.rb'
require 'flows_distributions/constant_distribution.rb'
require 'flows_distributions/felix_distribution.rb'
require 'flows_distributions/normal_distribution.rb'
require 'flows_distributions/pareto_distribution.rb'
require 'flows_distributions/split_distribution.rb'
require 'builders_examples/pdm_builders/PhaseIWithPriorityQueues/Flow_concrete_builder.rb' 


RSpec.describe FlowConcreteBuilder do
    describe 'build_parameter_flow_distribution' do
        let(:dummy_class) { Class.new { include FlowConcreteBuilder } }
        context 'felixDistribution' do
            it 'builds the expected output' do
                my_distribution = FelixDistribution.new ExponentialDistribution.new("1/(10*M)"), 
                                                        "FELIX_MODE_HIGH_THROUGHOUT",
                                                        ConstantDistribution.new("TCP_MTU_bytes * 8"),
                                                        NormalDistribution.new("1*k", "1*k"),
                                                        "1*M",
                                                        1,
                                                        "TCP_MTU_bytes"

                output = dummy_class.new.build_parameter_flow_distribution('Flow0_1', my_distribution)
                expect(output.gsub( /\s+/, " " )).to eq(" flowFlow0_1.period =  DISTRIBUTION_FELIX;
                                        flowFlow0_1.period_period = DISTRIBUTION_EXPONENTIAL;
                                        flowFlow0_1.period_period_mu = 1/(10*M);
                                        flowFlow0_1.period_mode = FELIX_MODE_HIGH_THROUGHOUT;
                                        flowFlow0_1.period_size_bytes = DISTRIBUTION_NORMAL;
                                        flowFlow0_1.period_size_bytes_mu = 1*k ;
                                        flowFlow0_1.period_size_bytes_var = 1*k;
                                        flowFlow0_1.period_buffer_bytes = 1 * M;
                                        flowFlow0_1.period_timeout = 1;
                                        flowFlow0_1.period_out_size_bytes = TCP_MTU_bytes;
                                        flowFlow0_1.packetSize =  DISTRIBUTION_CONSTANT;  // (in bits)
                                        flowFlow0_1.packetSize_value = TCP_MTU_bytes * 8".gsub( /\s+/, " " ))
            end
        end
    end
end