require 'spec_helper'
require 'topology_generator.rb' 

require 'templates/PhaseI/builders/pdm/pdm_constants.rb'

RSpec.describe TopologyGenerator do

  describe '#initialize' do
    context 'when arguments are not well received' do
      before :each do
        @arguments = double
      end

      it 'fails if no arguments are received' do
        expect { TopologyGenerator.new nil }.to raise_error ArgumentError, 'No arguments received'
      end

      it 'fails if arguments does not implement source method' do
        allow(@arguments).to receive(:template_directory)
        allow(@arguments).to receive(:output_directory)
        expect { TopologyGenerator.new @arguments }.to raise_error ArgumentError, 'It is mandatory that arguments has a source'  
      end

      it 'fails if arguments does not implement template_directory' do
        allow(@arguments).to receive(:source)
        allow(@arguments).to receive(:output_directory)
        expect { TopologyGenerator.new @arguments }.to raise_error ArgumentError, 'It is mandatory that arguments has a template_directory'  
      end

      it 'fails if arguments does not implement output_directory' do
        allow(@arguments).to receive(:source)
        allow(@arguments).to receive(:template_directory)
        expect { TopologyGenerator.new @arguments }.to raise_error ArgumentError, 'It is mandatory that arguments has a output_directory'  
      end
    end

    context 'when ONOS is recieved as source' do
      before :each do
        @arguments = double
        allow(@arguments).to receive(:source).and_return 'ONOS'
        allow(@arguments).to receive(:template_directory)
        allow(@arguments).to receive(:output_directory)
      end

      it 'fails when no uri_resource is specified' do
        expect{ TopologyGenerator.new @arguments }.to raise_error ArgumentError, 'It is mandatory that arguments has a uri_resource'  
        allow(@arguments).to receive(:source).and_return 'CUSTOM'
        expect{ TopologyGenerator.new @arguments }.to raise_error ArgumentError, 'It is mandatory that arguments has a uri_resource'  
      end

      it 'creates an OnosTopologyProvider' do
        allow(@arguments).to receive(:uri_resource).and_return 'pepito'
        allow(OnosTopologyProvider).to receive(:new).and_return OnosTopologyProvider.new @arguments.uri_resource

        my_topo_gen = TopologyGenerator.new @arguments
        expect(OnosTopologyProvider).to have_received(:new).with('pepito').once
        expect(my_topo_gen.topology_provider).to be_instance_of(OnosTopologyProvider)
      end
    end

    context 'when CUSTOM is recieved as source' do
      it 'creates a CustomTopologyProvider' do
        @arguments = double
        @uri_resource = 'pepito'
        customTopologyProviderInstance = class_double('CustomTopologyProvider')

        allow(@arguments).to receive(:source).and_return 'CUSTOM'
        allow(@arguments).to receive(:template_directory)
        allow(@arguments).to receive(:output_directory)
        allow(@arguments).to receive(:uri_resource).and_return @uri_resource

        allow(CustomTopologyProvider).to receive(:new).and_return customTopologyProviderInstance

        my_topo_gen = TopologyGenerator.new @arguments
        expect(CustomTopologyProvider).to have_received(:new).once
        expect(my_topo_gen.topology_provider).to be_instance_of(CustomTopologyProvider)
      end
    end
  end

  describe '#generate' do
    before :each do
      @pdm_builder = double
      @flows_builder = double
      @arguments = double
      @topology = double
      @topology_provider = double

      allow(@arguments).to receive(:source).and_return 'CUSTOM'
      allow(@arguments).to receive(:uri_resource).and_return 'pepito'
      allow(@arguments).to receive(:template_directory).and_return 'pepito'
      allow(@arguments).to receive(:output_directory).and_return 'juancito'
      
      allow(@topology_provider).to receive(:get_topology).and_return @topology

      allow(PdmBuilder).to receive(:new).and_return @pdm_builder
      allow(@pdm_builder).to receive(:buildPDM)

      allow(FlowsBuilder).to receive(:new).and_return @flows_builder
      allow(@flows_builder).to receive(:build_simulation_files_parameters)

      @topology_generator = TopologyGenerator.new @arguments
      allow(@topology_generator).to receive(:topology_provider).and_return @topology_provider
    end

    context 'building' do
      it 'calls the PDM builder with the template_directory and output_directory recieved by argument' do
        @topology_generator.generate

        expect(PdmBuilder).to have_received(:new).with(@arguments.template_directory, @arguments.output_directory).once
        expect(@pdm_builder).to have_received(:buildPDM).with(@topology, PDM_INITIAL_STRUCTURE, PDM_FINAL_STRUCTURE, NUMBER_OF_PDM_MODELS_IN_STRUCTURE)
      end

      it 'calls the flows builder with the topology, the provider and the output_directory' do
        @topology_generator.generate

        expect(FlowsBuilder).to have_received(:new).with(@topology_provider, @arguments.template_directory, @arguments.output_directory).once
        expect(@flows_builder).to have_received(:build_simulation_files_parameters).with(no_args).once
      end
    end
  end

end