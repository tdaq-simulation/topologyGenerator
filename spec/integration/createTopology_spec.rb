require 'spec_helper'
require 'fileutils'
require 'topology_generator.rb' 
require 'command_line/command_line_arguments.rb'

PDPPT_PATH = File.dirname(__FILE__) + '/../bin/pdppt' 

RSpec.describe 'createTopologyScript' do

  describe 'topologyGeneration' do

    context "with CUSTOM provider" do
      before(:each) do
        @arguments = double CommandLineArguments
        @output_dir = "./lib/output"
        allow(@arguments).to receive(:source).and_return "CUSTOM"
        allow(@arguments).to receive(:uri_resource).and_return "lib/templates/PhaseI/networks_topologies/phaseI_topology.rb"
        allow(@arguments).to receive(:output_directory).and_return @output_dir
        allow(@arguments).to receive(:template_directory).and_return(File.dirname(__FILE__) + '/../../lib/templates/PhaseI')
        
      end

      it "generates files that can be read by PDM compiler" do
        topo_gen = TopologyGenerator.new @arguments
        topo_gen.generate
        
        # check that files were generated
        expect(File.directory?(@output_dir)).to equal(true)
        expect(File.file?(@output_dir + "/topology.pdm")).to equal(true)
        expect(File.file?(@output_dir + "/flows_definition.scilabParams")).to equal(true)
        expect(File.file?(@output_dir + "/FlowDefinitions.cpp")).to equal(true)
          
        # check that the topology can be parsed by scilab
        cmd = PDPPT_PATH + " -c -s \"#{@output_dir}/topology.pdm\" 2> /dev/null"
        was_good = system(cmd) #TODO: this is printing in the console 'Finished code generation...' which might be anoying. Using "2>&1 /dev/null" makes the script to fail 
        expect(was_good).to equal(true)
        
        # clean up
        FileUtils.rm_rf(@output_dir)       
      end

    end  
  end
end