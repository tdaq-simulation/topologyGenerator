require 'spec_helper'
require 'providers/interface_topology_provider.rb'
require 'providers/apis/onos_topology_provider.rb' 

RSpec.describe OnosTopologyProvider, "#initialize" do
	context "with no uri_resource recieved as parameter" do
		it "raises and ArgumentError exception" do
			expect { OnosTopologyProvider.new nil }.to raise_error ArgumentError, 'No uri recieved as parameter'
		end
	end
end