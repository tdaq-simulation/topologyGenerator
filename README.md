# TODO: complete with instalation and usage instructions

Dependecies
--------------
- Ruby 2.3.0
   - Install ruby version manager: https://www.digitalocean.com/community/tutorials/how-to-install-ruby-on-rails-on-centos-6-with-rvm
   - install ruby 2.3: - rvm install 2.3.0
                       - rvm use 2.3.0 --default 
   - verify ruby version: ruby --version
                          --> ruby 2.3.0p0 (2015-12-25 revision 53290) [x86_64-linux]

- Bundle: gem install bundler

SETUP
-------
- bundle install

OUTPUT
-------
- .pdm : This file contains the graphical and structure representation for loading the simulation in powerDevs.

- FlowDefinitions.cpp : This cpp contains the definition of each of the flows that will be run in the simulation.

- flows_definition.scilabParams : This file contains the parameters necessary for each flow defined in FlowDefinitions.cpp to run in the simulation. 


EXECUTE:
---------

- The script has an option for specifying where the topology data should be retrieved, and for specifying the pdm output file. Examples of use are the following:
    - Use as data source a CUSTOM  provider (-n) with uri network_topologies_examples/tdaq_topology_example.rb (-d), and the builder defined in PhaseIWithPriorityQueues (-u). Output is generated in the PowerDEVS structure (-o)

            ./createTopology.rb source -n CUSTOM -d "builders_examples/pdm_builders/PhaseIWithPriorityQueues/" -u"network_topologies_examples/tdaq_topology_example.rb" -o"/home/mbonaven/gitlab/powerdevs/examples/PhaseI_topologies/only_LAr"

    - Use as data source a CUSTOM provider and the builder defined in PhaseIWithPriorityQueues (an .rb file uri will be requested)

            ./createTopology.rb source -n CUSTOM -d "builders_examples/pdm_builders/PhaseIWithPriorityQueues/"            
   
    - Use as data source the ONOS controller api. Since there is no output file specified, it will be written in the default file called my_topology.pdm

            ruby createTopology.rb -n ONOS 

    - Use as data source the ONOS controller api, and output the file in my_topology

            ruby createTopology.rb -n ONOS -o output_path


Helper scripts:
  - copy files:  ../bin/copy_file_to_powerdevs.sh   (NOTE: remember to change the paths in these script TODO: receive them as parameters)


NOTEs: 
- remember to update the model.scilabParams to load the generated scilab files (this needs to be done only once in the model)
- remember to update the LoadScilabParams and FinalizationCommands models to point to the generated files.


TODOs:
- BUG: using the builder PDM with prioOutput, if there is an outport that is not connected (no link), the link parameters are not generated but the link model is generated (error initializing simulation).    
- the generator is not aware of the powerdevs model path, thus: 
            1) it does not generate files in the correct places (copying is needed). 
            2) the model.scilabParams file is not updated to point to the generated files
            3) the LoadScilabParams and FinalizationCommands models has the path hardcoded (need to be updated)
- tests are not passing: several out-dated references to 'builders' folder so spec does not run. Some tests execute and fail 
- generate the metrics and plotting files. To start with basic stuff (ej: log everything, plot receibing throughput). Then we will add as needed like queue occupation.
 