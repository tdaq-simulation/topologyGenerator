#!/bin/bash

export POWERDEVS_PATH=/home/mbonaven/gitlab/powerdevs
export POWERDEVS_PROJECT_PATH=$POWERDEVS_PATH/examples/PhaseI_topologies/only_LAr/
export POWERDEVS_ATOMICS=$POWERDEVS_PATH/atomics/PhaseI
export GENERATED_FILES_PATH=/home/mbonaven/gitlab/powerdevs/examples/PhaseI_topologies/only_LAr/

echo cp $GENERATED_FILES_PATH/topology.pdm $POWERDEVS_PROJECT_PATH

rm $POWERDEVS_PROJECT_PATH/topology.pds $POWERDEVS_PROJECT_PATH/topology.stm
cp $GENERATED_FILES_PATH/topology.pdm $POWERDEVS_PROJECT_PATH
cp $GENERATED_FILES_PATH/FlowDefinitions.cpp $POWERDEVS_ATOMICS
cp $GENERATED_FILES_PATH/flows_definition.scilabParams $POWERDEVS_PROJECT_PATH/Scilab
cp $GENERATED_FILES_PATH/links_definition.scilabParams $POWERDEVS_PROJECT_PATH/Scilab
cp $GENERATED_FILES_PATH/routers_definition.scilabParams $POWERDEVS_PROJECT_PATH/Scilab
cp $GENERATED_FILES_PATH/hosts_definition.scilabParams $POWERDEVS_PROJECT_PATH/Scilab


#compile
